﻿var login = login || {}

login.methods = {
    verifyFields: () => {
        let user = document.getElementById('user_fg_field').value
        let pass = document.getElementById('pass_fg_field').value

        if (user == '') {
            login.methods.handlerErr({
                msg: 'Usuário não pode ser vazio',
                type: 'invalid',
                id: 'user_fg'
            })
        }
        if (pass == '') {
            login.methods.handlerErr({
                msg: 'Senha não pode ser vazio',
                type: 'invalid',
                id: 'pass_fg'
            })
        }

        if (user == 'admin' && pass == 'admin') {
            // redirecionamento
        }
    },
    handlerErr: ({ msg, type, id  }) => {

        let el_status = document.createElement('div')
        el_status.classList.add(type + '-feedback')
        el_status.innerHTML = msg

        let input = document.getElementById(id + '_field')
        input.classList.add('is-' + type) 

        let content = document.getElementById(id)
        content.appendChild(el_status)

        setInterval(function () {
            input.classList.remove('is-' + type)
        }, 3000)
    }
}

login.listeners = {
    _submit: () => {
        let b = document.getElementById('btSubmit')

        b.addEventListener('click', () => {
            login.methods.verifyFields();
        })
    }
}

login.running = () => {
    login.listeners._submit();
}

login.running();