﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataBase;

namespace Base.DataBase
{
    [Table("Cliente")]
    public class Cliente : DefaultDatabase<Cliente>
    {
        public int id { get; set; }
        public string cpf { get; set; }
        public string contato { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public DateTime dataNascimento { get; set; }


        public List<Cliente> Listar()
        {
            return ExecuteQuery<Cliente>(@"SELECT * FROM vini.Cliente");
        }

    }
}
